//Kiem tra rong
function kiemTraRong(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerText =
      "Trường này không được để rỗng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

// Kiem tra ma Sv da ton tai
function kiemTraMaSv(idSv, listSv, idError) {
  var index = listSv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Mã Sinh Viên đã tồn tại";
    return false;
  }
}

// Kiem tra So
function kiemTraSo(idSv, idError) {
  const regNumber = /^\d+$/;
  // var reg = new RegExp('^[0-9]$');
  var isNumber = regNumber.test(idSv);
  if (!isNumber) {
    document.getElementById(idError).innerText = "Mã Sinh Viên Phải là số";
    return false;
  } else {
    if (idSv.length >= 4 && idSv.length <= 6) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Mã Sinh Viên Phải chứa 4 đến 6 ký số";
      return false;
    }
  }
}

//Kiem tra chuoi Ky tu
function kiemTraKyTu(tenSv, idError) {
  const regCharactersAndSpaces = /^[A-Za-z\s]*$/;
  var isLetter = regCharactersAndSpaces.test(tenSv);
  console.log("isLetter: ", isLetter);
  if (!isLetter) {
    document.getElementById(idError).innerText = "Tên Sinh Viên Phải là chữ";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

//Kiem tra email
function kiemTraEmail(email, idError) {
  const regEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = regEmail.test(email);
  if (!isEmail) {
    document.getElementById(idError).innerText =
      "Nhập email đúng định dạng xxx@xxx.xxx";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

//Kiem tra Mat Khau
function kiemTraMatKhau(matKhau, idError) {
  if (matKhau.length >= 6 && matKhau.length <= 10) {
    var haveNumber = matKhau.match(/\d/g, "");
    var haveUppercaseCharacter = matKhau.match(/[A-Z]/g, "");
    var haveregSpecialChar = matKhau.match(
      /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/g,
      ""
    );
    if (
      haveNumber == null ||
      haveUppercaseCharacter == null ||
      haveregSpecialChar == null
    ) {
      document.getElementById(idError).innerText =
        "Mật Khẩu phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  } else {
    document.getElementById(idError).innerText =
      "Mật Khẩu phải chứa ít nhất 6 đến 10 ký tự";
    return false;
  }
}
//Kiem tra diem toan ly hoa
function kiemTraDiem(diem, idError) {
  if (diem >= 0 && diem <= 10) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Điểm phải từ 0 đến 10";
    return false;
  }
}
