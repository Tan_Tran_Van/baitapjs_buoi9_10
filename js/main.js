var dssv = [];
const DSSV = "DSSV";
//show data dssv tu localstorage
var dataJson = localStorage.getItem(DSSV);
if (dataJson) {
  var dataRaw = JSON.parse(dataJson);
  dssv = dataRaw.map(function (sv) {
    return new SinhVien(
      sv.ma,
      sv.ten,
      sv.email,
      sv.matKhau,
      sv.diemToan,
      sv.diemLy,
      sv.diemHoa
    );
  });
  renderDssv(dssv);
}

//luu xuong localStorage
function saveLocalStorage(studentList) {
  var dssvJson = JSON.stringify(studentList);
  localStorage.setItem(DSSV, dssvJson);
}
// Them Sinh Vien
document.getElementById("btnThemSv").onclick = function () {
  var newSv = layThongTinTuForm();

  //validate
  var isValid = kiemTra(newSv);
  //Kiem tra ma SV
  isValid &=
    kiemTraRong(newSv.ma, "spanMaSV") &&
    kiemTraMaSv(newSv.ma, dssv, "spanMaSV") &&
    kiemTraSo(newSv.ma, "spanMaSV");
  //
  if (isValid) {
    dssv.push(newSv);
    saveLocalStorage(dssv);
    renderDssv(dssv);
    resetForm("formQLSV");
  }
};
//Xoa Sinh Vien
function xoaSv(idSv) {
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) return;
  dssv.splice(index, 1);
  saveLocalStorage(dssv);
  renderDssv(dssv);
  resetForm("formQLSV");
}
//Sua Sinh Vien
function suaSv(idSv) {
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) return;
  showThongTinLenForm(dssv[index]);

  // Tat input maSv va button themSV, reset
  disabledInput("txtMaSV");
  disabledInput("btnThemSv");
}
// Cap nhat Sinh vienresetValueValid
document.getElementById("btnCapNhat").onclick = function () {
  var svEdit = layThongTinTuForm();
  //validate
  var isValid = kiemTra(svEdit);
  //
  if (isValid) {
    var index = dssv.findIndex(function (sv) {
      return sv.ma == svEdit.ma;
    });
    if (index == -1) return;
    dssv[index] = svEdit;
    saveLocalStorage(dssv);
    renderDssv(dssv);
    resetForm("formQLSV");
    //
    enabledInput("txtMaSV");
    enabledInput("btnThemSv");
  }
};
//Reset form
document.getElementById("btnReset").onclick = function () {
  resetForm("formQLSV");
  enabledInput("txtMaSV");
  enabledInput("btnThemSv");
  renderDssv(dssv);
  resetValueValid();
  document.getElementById("txtSearch").value = "";
};

//Search SV
document.getElementById("btnSearch").onclick = function () {
  var tenSV = document.getElementById("txtSearch").value;
  var timTenSv = dssv.filter(function (sv) {
    return sv.ten.indexOf(tenSV) > -1;
  });
  renderDssv(timTenSv);
};
